package com.warathip.week6;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        BookBank Warathip = new BookBank("Warathip",50.0);
        
        Warathip.print();
        BookBank Kai = new BookBank("Kai",100.0);
        Kai.print();
        Kai.withdraw(70.0);
        Kai.print();

        Warathip.deposit(70.0);
        Warathip.print();
    }
}
