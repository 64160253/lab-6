package com.warathip.week6;

public class RoBot {
    private String name;
    private char symbol;
    private int x;
    private int y;
    public static final int Min_X = 0;
    public static final int Min_Y = 0;
    public static final int Max_X = 19;
    public static final int Max_Y = 19;

    public RoBot(String name,char symbol,int x ,int y){
        this.name = name;
        this.symbol = symbol;
        this.x = x;
        this.y = y;
    }
    public RoBot(String name,char symbol){
        this(name, symbol, 0,0);
    }
    public boolean up(){
        if(y==Min_Y) return false;
        this.y = this.y-1;
        return true;
    }
    public boolean up(int step){
        for(int i=0; i<step; i++){
            if(up() == false){
                return false;
            }
        }
        return true;
    }
    public boolean down(){
        if(y==Max_Y) return false;
        y = y+1;
        return true;
    }
    public boolean down(int step){
        for(int i=0; i<step; i++){
            if(down() == false){
                return false;
            }
        }
        return true;
    }
    public boolean left(){
        x = x-1;
        return true;
    }
    public boolean left(int step){
        for(int i=0; i<step; i++){
            if(down() == false){
                return false;
            }
        }
        return true;
    }
    public boolean right(){
        x = x+1;
        return true;
    }
    public boolean right(int step){
        for(int i=0; i<step; i++){
            if(down() == false){
                return false;
            }
        }
        return true;
    }
    public void print(){
        System.out.println("RoBot:" +  name  +  "x:"  +  x  +  "y:"  +  y );
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return name;
    }
    public char getSymbol(){
        return symbol;
    }
    public int getx(){
        return x;
    }
    public int gety(){
        return y;
    }
}
