package com.warathip.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {
    @Test
    public void shouldWithDrawSuccess(){
        BookBank book = new BookBank("Warathip", 200.0);
        book.withdraw(140.0);
        assertEquals(60, book.getBalance(),0.0001);
    }

    @Test
    public void shouldWithDrawOverBalance(){
        BookBank book = new BookBank("Warathip", 200.0);
        book.withdraw(220.0);
        assertEquals(200, book.getBalance(),0.0001);
    }

    @Test
    public void shouldWithNagativeNumber(){
        BookBank book = new BookBank("Warathip", 200.0);
        book.withdraw(-200.0);
        assertEquals(200, book.getBalance(),0.0001);
    }

    @Test
    public void shouldDeposit(){
        BookBank book = new BookBank("Warathip", 200.0);
        book.deposit(40.0);
        assertEquals(240, book.getBalance(),0.0001);
    }

}
