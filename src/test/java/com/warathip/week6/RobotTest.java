package com.warathip.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void ShouldDownOver(){
        RoBot robot = new RoBot("Robot", 'R', 0, RoBot.Max_Y);
        robot.down();
        assertEquals(false, robot.down());
        assertEquals(RoBot.Max_Y, robot.gety());
        }
        @Test
        public void ShouldCreateRobotSuccess1(){
            RoBot robot = new RoBot("Robot",'R');
            robot.down();
        assertEquals(true, robot.down());
        assertEquals(2, robot.gety());
        }

        @Test
    public void ShouldDownNegative(){
        RoBot robot = new RoBot("Robot", 'R', 0, RoBot.Min_Y);
        assertEquals(false, robot.up());
        assertEquals(RoBot.Min_Y, robot.gety());
        }


        @Test
    public void ShouldDownSuccess(){
        RoBot robot = new RoBot("Robot", 'R', 0, 0);
        assertEquals(true, robot.down());
        assertEquals(1, robot.gety());
        }

        @Test
        public void ShouldUpSuccess(){
            RoBot robot = new RoBot("Robot", 'R', 0, 1);
            assertEquals(true, robot.up());
            assertEquals(0, robot.gety());
            }

            @Test
        public void ShouldRightSuccess(){
            RoBot robot = new RoBot("Robot", 'R', 1, 0);
            assertEquals(true, robot.right());
            assertEquals(0, robot.gety());
            }
            
            @Test
        public void ShouldLeftSuccess(){
            RoBot robot = new RoBot("Robot", 'R', 0, 0);
            assertEquals(true, robot.left());
            assertEquals(0, robot.gety());
            }

            @Test
        public void ShouldUpNSuccess(){
            RoBot robot = new RoBot("Robot", 'R', 10, 11);
            assertEquals(true, robot.up(5));
            assertEquals(6, robot.gety());
            }

            @Test
    public void ShouldDownNSuccess(){
        RoBot robot = new RoBot("Robot", 'R', 10, 110);
        assertEquals(true, robot.down(4));
        assertEquals(114, robot.gety());
        }

        @Test
        public void ShouldRightNSuccess(){
            RoBot robot = new RoBot("Robot", 'R', 10, 11);
            assertEquals(true, robot.right(4));
            assertEquals(15, robot.gety());
            }
            @Test
        public void ShouldLeftNSuccess(){
            RoBot robot = new RoBot("Robot", 'R', 10, 11);
            assertEquals(true, robot.left(4));
            assertEquals(15, robot.gety());
            }

}
